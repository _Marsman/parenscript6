;;;; parenscript6.lisp

(in-package #:parenscript6)

(defmacro setup ()
  "Run this at the beginning of your code."
  `(stringify "var __PS_MV_REG = []; "))

(defmacro+ps ps-expression (&rest expr)
  `(string-right-trim ";" (ps ,@expr)))

(defmacro+ps ps-bind (&rest methods)
  "Generates this.method_name = this.method_name.bind(this). Use it inside of the constructor."
  (let (result)
    (dolist (el methods)
      (push (list 'setf (list '@ 'this el) (list 'chain 'this el '(bind this))) result))
    `(progn ,@result)))

(defmacro parse-arguments (arguments)
  "Turns a Lisp parameter list into a JavaScript parameter list. (x *ze-ro) -> (x, ZeRo)"
  `(string-left-trim "new a" (ps-expression (new (a ,@arguments)))))

(defmacro ps=> (arguments &body body)
  "ES6 Arrow Functions"
  `(stringify (parse-arguments ,arguments) " => "
              (string-left-trim "function (a) " (ps #'(lambda (a) ,@body)))))

(defmacro new-method (name arguments &body body)
  "Defines a new class method for a Parenscript Class."
  `(stringify (ps-expression ,name) (parse-arguments ,arguments) " "
              (string-left-trim "function (a) " (ps #'(lambda (a) ,@body))) " "))

(defmacro new-method-arrow (name arguments &body body)
  "Defines a new class method for a Parenscript Class with an Arrow Function."
  `(stringify (ps-expression ,name) " = " (ps=> ,arguments ,@body) " "))

(defmacro getter (name arguments &body body)
  `(stringify "get " (new-method ,name ,arguments ,@body)))

(defmacro setter (name arguments &body body)
  `(stringify "set " (new-method ,name ,arguments ,@body)))

(defmacro static (&body body)
  `(stringify "static " ,@body))

(defmacro const (&body body)
  `(stringify "const " ,@body))

(defmacro ps6-var (&body body)
  `(stringify "var " ,@body))

(defmacro ps-setf (var ps-expression)
  "Use inside new-class."
  `(stringify (ps-expression ,var) " = " (ps ,ps-expression)))

(defmacro ps6-setf (var code)
  "Like ps-setf but without ps-expression. You can use an Arrow Function (ps=>)"
  `(stringify (ps-expression ,var) " = " ,code))

(defmacro new-class (name (&key extends constructor) &body body)
  "Creates a new Parenscript Class"
  `(stringify "class " (ps-expression ,name) " "
                  ,(if extends `(stringify "extends " (ps-expression ,extends)) "")
                  " { "
                  ,(if constructor `(stringify "constructor"
                                                   (parse-arguments ,(first constructor))
                                                   " { "
                                                   (ps ,@(second constructor))
                                                   " }; ") "")
                  ,@body
                  " }; "))




;; React

(defmacro+ps jsx (tag &optional props-values (content '(null)))
  `((@ -react create-element) ,tag
    ,(if props-values `(create  ,@props-values) 'null)
    ,@content))

(defmacro+ps r-props (&rest properties)
  "Access Properties from this.props"
  `(@ this props ,@properties))

(defmacro+ps r-state (&rest properties)
  "Acces Properties from this.state"
  `(@ this state ,@properties))

(defmacro+ps r-data (&rest properties)
  "Access data from props.data (functional components)"
  `(@ props data ,@properties))

(defmacro+ps with-use-state (default-value getter &optional setter)
  "Macro for binding variables to the result of a useState React Hook."
  `(progn
     (var ,getter)
     ,(if setter `(var ,setter))
     (let ((use-state-array (use-state ,default-value)))
       (setf ,getter (aref use-state-array 0))
       ,(if setter `(setf ,setter (aref use-state-array 1))))))
