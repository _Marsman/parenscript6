;;;; parenscript6.asd

(asdf:defsystem #:parenscript6
  :description "Adds some ES6 features to Parenscript."
  :author "Marsman"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "parenscript6"))
  :depends-on (:parenscript))
